# -*- coding:utf8 -*-
import os
import pymongo
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web


from tornado.options import define, options

define("port", default=9998, help="run on the given port", type=int)
define("dbhost", default='192.168.100.6')
define("dbport", default=27017, help="run on the given port", type=int)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        #Connect to Mongodb
        conn = pymongo.Connection(host=options.dbhost, port=options.dbport)
        #select db
        db = conn.monitor
        #select connection
        monitorlog = db.monitorlog
        servers = monitorlog.distinct('serverip')
        print servers
        server_info = []
        for server in servers:
            info = monitorlog.find({'serverip' : server}).sort('time', -1 ).limit(1)
            server_info.append(info)

        #for info in server_info:
        #    print info 
        self.render("index.html", servers=servers, server_info=server_info)

class DetailHandler(tornado.web.RequestHandler):
    def get(self, argv):
        serverip = argv
        print serverip
        conn = pymongo.Connection(host=options.dbhost, port=options.dbport)
        #select db
        db = conn.monitor
        #select connection
        monitorlog = db.monitorlog
        server_info = monitorlog.find({'serverip' : serverip}).sort('time', -1 ).limit(120)
        #print server_info
        categories = []
        oneM = []
        fiveM = []
        fifteenM = []
        cpuused = []
        memory = []
        memTotalReal = []
        memAvailReal = []
        memTotalSwap = []
        memUsedSwap = []
        memAvailSwap = []
        dskTotal = []
        dskUsed = []
        dskAvail = []
        tcps = []
        udps = []
        connections = []
        processnum = []
        loginusers = []
        for item in server_info:
            categories.append(str(item['time'])[:-7])
            oneM.append(float(item['oneM']))
            fiveM.append(float(item['fiveM']))
            fifteenM.append(float(item['fifteenM']))
            cpuused.append(item['cpuused'])
            memory.append(int(item['memUsedReal']) / 1024)
            memTotalReal.append(int(item['memTotalReal']) /1024)
            memAvailReal.append(int(item['memAvailReal']) / 1024)
            memTotalSwap.append(int(item['memTotalSwap']) / 1024)
            memUsedSwap.append(int(item['memUsedSwap']) / 1024)
            memAvailSwap.append(int(item['memAvailSwap']) / 1024)
            dskTotal.append(int(item['dskTotal']) / 1024 / 1024)
            dskUsed.append(int(item['dskUsed']) / 1024 / 1024)
            dskAvail.append(int(item['dskAvail']) / 1024 / 1024)
            tcps.append(int(item['tcps']))
            udps.append(int(item['udps']))
            connections.append(int(item['tcps']) + int(item['udps']))
            processnum.append(int(item['processnum']))
            loginusers.append(int(item['loginusers']))
            

        self.render("detail.html", serverip=serverip, categories=categories[::-1], \
            oneM=oneM, fiveM=fiveM, fifteenM=fifteenM, cpuused=cpuused, \
            memory=memory, memTotalReal=memTotalReal, memAvailReal=memAvailReal, \
            memTotalSwap=memTotalSwap, memUsedSwap=memUsedSwap, memAvailSwap=memAvailSwap, \
            dskTotal=dskTotal, dskUsed=dskUsed, dskAvail=dskAvail, connections=connections, \
            tcps=tcps, udps=udps, processnum=processnum, loginusers=loginusers)

def main():
    settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
        )

    tornado.options.parse_command_line()
    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/detail/(.*)", DetailHandler),
    ], **settings)
    
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
    

if __name__ == "__main__":
    main()
    
