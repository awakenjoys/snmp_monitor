# -*- coding:utf8 -*-
import netsnmp
import datetime
import time
from pymongo import MongoClient

#---------------------------------------------------------------------------------------
#获取启动时间
def getuptime(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('hrSystemUptime.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)  
	result['uptime'] = ret[0]

	return result

#---------------------------------------------------------------------------------------
#获取服务器时间
def getsystemdate(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('hrSystemDate.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['systemdate'] = systemdate
	return result

#---------------------------------------------------------------------------------------
#获取系统负载
def getload(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('laLoad.1')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)  
	result['oneM'] = ret[0]

	var = netsnmp.Varbind('laLoad.2')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['fiveM'] = ret[0]

	var = netsnmp.Varbind('laLoad.3')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['fifteenM'] = ret[0]

	return result

#---------------------------------------------------------------------------------------
#获取登陆的用户
def getloginusers(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('hrSystemNumUsers.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['loginusers'] = ret[0]

	return result

#---------------------------------------------------------------------------------------
#获取进程数
def getprocessnum(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('hrSystemProcesses.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['processnum'] = ret[0]

	return result

#---------------------------------------------------------------------------------------
#获取CPU占用情况
def getcpuusage(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('ssCpuUser.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['cpuuser'] = ret[0]

	var = netsnmp.Varbind('ssCpuSystem.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['cpusystem'] = ret[0]

	var = netsnmp.Varbind('ssCpuIdle.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['cpuidle'] = ret[0]

	result['cpuused'] = 100 - float(result['cpuidle'])

	return result

#---------------------------------------------------------------------------------------
#获取内存使用情况
def getmemory(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('memTotalReal.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['memTotalReal'] = ret[0]

	var = netsnmp.Varbind('memAvailReal.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['memAvailReal'] = ret[0]

	var = netsnmp.Varbind('memTotalFree.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['memTotalFree'] = ret[0]

	var = netsnmp.Varbind('memTotalSwap.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['memTotalSwap'] = ret[0]

	var = netsnmp.Varbind('memAvailSwap.0')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['memAvailSwap'] = ret[0]

	result['memUsedReal'] = float(result['memTotalReal']) - float(result['memAvailReal'])
	result['memUsedSwap'] = float(result['memTotalSwap']) - float(result['memAvailSwap'])

	return result

#---------------------------------------------------------------------------------------
#获取磁盘空间情况
def getdisk(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('dskTotal.1')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['dskTotal'] = ret[0]

	var = netsnmp.Varbind('dskAvail.1')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['dskAvail'] = ret[0]

	var = netsnmp.Varbind('dskUsed.1')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['dskUsed'] = ret[0]

	var = netsnmp.Varbind('dskPercent.1')
	ret = netsnmp.snmpget(var, Version=ver, DestHost=host, Community=passwd)
	result['dskPercent'] = ret[0]

	return result

#---------------------------------------------------------------------------------------
#获取网卡信息
def getinterface(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('ifDescr')
	ret = netsnmp.snmpwalk(var, Version=ver, DestHost=host, Community=passwd)
	ifcard = []
	for ifDescr in ret:
		ifcard.append(ifDescr)

	var = netsnmp.Varbind('ifOperStatus')
	ret = netsnmp.snmpwalk(var, Version=ver, DestHost=host, Community=passwd)
	ifactive = []
	for ifStatus in ret:
		ifactive.append(ifStatus)

	result['interface'] = dict(zip(ifcard, ifactive))

	var = netsnmp.Varbind('ifHCInOctets')
	ret = netsnmp.snmpwalk(var, Version=ver, DestHost=host, Community=passwd)
	ifInOctets = []
	for InOctet in ret:
		ifInOctets.append(InOctet)

	result['_in'] = dict(zip(ifcard,ifInOctets))

	var = netsnmp.Varbind('ifHCOutOctets')
	ret = netsnmp.snmpwalk(var, Version=ver, DestHost=host, Community=passwd)
	ifOutOctets = []
	for OutOctet in ret:
		ifOutOctets.append(OutOctet)
	result['out'] = dict(zip(ifcard,ifOutOctets))
	return result

#---------------------------------------------------------------------------------------
#获取连接状态
def connstatus(ver, host, passwd):
	result = {}
	var = netsnmp.Varbind('tcpConnLocalPort')
	ret = netsnmp.snmpwalk(var, Version=ver, DestHost=host, Community=passwd)
	result['tcps'] = len(list(ret))
	var = netsnmp.Varbind('udpLocalPort')

	ret = netsnmp.snmpwalk(var, Version=ver, DestHost=host, Community=passwd)
	result['udps'] = len(list(ret))
	return result

#---------------------------------------------------------------------------------------
#获取全部信息
def getallinfo(ver, hostip, passwd):
	info = {}
	ret = getload(ver, hostip, passwd)			#系统负载
	info.update(ret)
	ret = getloginusers(ver, hostip, passwd)	#登陆用户数
	info.update(ret)
	ret = getprocessnum(ver, hostip, passwd)	#进程数
	info.update(ret)
	ret = getcpuusage(ver, hostip, passwd)		#CPU占用率
	info.update(ret)
	ret = getmemory(ver, hostip, passwd)		#内存使用情况
	info.update(ret)
	ret = getdisk(ver, hostip, passwd)			#磁盘容量
	info.update(ret)
	ret = getinterface(ver, hostip, passwd)		#网卡、流量情况
	info.update(ret)
	ret = connstatus(ver, hostip, passwd)		#TCP、UDP连接数
	info.update(ret)
	ret = getuptime(ver, hostip, passwd)		#系统启动时间
	info.update(ret)
	#ret = getsystemdate(ver, hostip, passwd)	#系统时间
	#info.update(ret)
	info['time'] = datetime.datetime.now()		#时间
	info['serverip'] = hostip					#服务器IP

	return info

#---------------------------------------------------------------------------------------
#入库
def insertdb(info):
	client = MongoClient('localhost', 27017)
	monitor = client.monitor
	monitorlog = monitor.monitorlog
	monitorlog.insert(info)

#---------------------------------------------------------------------------------------
#主函数
if __name__ == '__main__':
	while True:
		#需要获取信息的服务器IP
		iplist = ['192.168.100.7', '192.168.100.6', '106.186.30.193']
		for ip in iplist:
			print "%s %s %s" % ("-" * 50, ip, "-" * 50) 
			insertdb(getallinfo(2, ip, 'hartnett'))
			print getallinfo(2, ip, 'hartnett')

		#获取信息时间间隔为5分钟
		time.sleep(60 * 5)
